function krylov_real_shifted_linear_equation_solve_projected(equation, orthonormalizer) result(error)

    use kinds, only: IK, RK
    use errors, only: OK, INVALID_DIMENSION
    use linalg, only: real_sy_solve_linear
    use krylov, only: real_shifted_linear_equation_t, real_orthonormalizer_t
    implicit none

    class(real_shifted_linear_equation_t), intent(inout) :: equation
    class(real_orthonormalizer_t), intent(inout) :: orthonormalizer
    integer(IK) :: error

    integer(IK) :: err, sol, vec
    real(RK), allocatable :: shifted(:, :), transformed_rayleigh(:, :), transformed_basis_rhs(:, :), solution(:)

    if (equation%basis_dim /= orthonormalizer%basis_dim) then
        error = INVALID_DIMENSION
        return
    end if

    allocate (shifted(equation%basis_dim, equation%basis_dim), &
              transformed_rayleigh(equation%basis_dim, equation%basis_dim), &
              transformed_basis_rhs(equation%basis_dim, equation%solution_dim), &
              solution(equation%basis_dim))

    do sol = 1_IK, equation%solution_dim

        shifted = equation%rayleigh
        do vec = 1_IK, equation%basis_dim
            shifted(vec, vec) = shifted(vec, vec) - equation%shifts(sol)
        end do

        err = orthonormalizer%transform_rayleigh(equation%basis_dim, shifted, transformed_rayleigh)
        if (err /= OK) then
            deallocate (shifted, transformed_rayleigh, transformed_basis_rhs, solution)
            error = err
            return
        end if

        err = orthonormalizer%transform_basis_rhs(equation%basis_dim, equation%solution_dim, equation%basis_rhs, &
                                              transformed_basis_rhs)
        if (err /= OK) then
        deallocate (shifted, transformed_rayleigh, transformed_basis_rhs, solution)
        error = err
        return
        end if

        err = real_sy_solve_linear(transformed_rayleigh, transformed_basis_rhs(:, sol), equation%basis_dim, 1_IK, solution)
        if (err /= OK) then
            deallocate (shifted, transformed_rayleigh, transformed_basis_rhs, solution)
            error = err
            return
        end if

        err = orthonormalizer%restore_basis_solutions(equation%basis_dim, 1_IK, solution, equation%basis_solutions(:, sol))
        if (err /= OK) then
            deallocate (shifted, transformed_rayleigh, transformed_basis_rhs, solution)
            error = err
            return
        end if

    end do

    deallocate (shifted, transformed_rayleigh, transformed_basis_rhs, solution)

    error = OK

end function krylov_real_shifted_linear_equation_solve_projected
